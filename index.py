import pandas as pd
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import keras
from sklearn.metrics import mean_absolute_error

from keras.layers import Dense, Activation
# from IPython.display import SVG
from keras.optimizers import Adam
from keras.utils.vis_utils import model_to_dot


headers = ['user_id', 'item_id', 'rating', 'timestamp']
ratings_df = pd.read_csv('data/ml-100k/ua.base',
                         sep='\t',
                         names=headers,
                         # header=header_row,
                         dtype={
                           'user_id': np.int32,
                           'item_id': np.int32,
                           'rating': np.float32,
                           'timestamp': np.int32,
                         })
# print(ratings_df.head())

# reindexing the users and items
ratings_df.user_id = ratings_df.user_id.astype('category').cat.codes.values
ratings_df.item_id = ratings_df.item_id.astype('category').cat.codes.values

print(ratings_df.head())

train, test = train_test_split(ratings_df, test_size=0.2)

n_users, n_movies = len(ratings_df.user_id.unique()), len(ratings_df.item_id.unique())
n_latent_factors = 3

movie_input = keras.layers.Input(shape=(1,), name='Item')
movie_embedding = keras.layers.Embedding(n_movies + 1, n_latent_factors, name='Movie-Embedding')(movie_input)
# flatten the embedding for using in layers
movie_vec = keras.layers.Flatten(name='FlattenMovies')(movie_embedding)

user_input = keras.layers.Input(shape=[1], name='User')
user_embedding = keras.layers.Embedding(n_users + 1, n_latent_factors, name='User-Embedding')(user_input)
user_vec = keras.layers.Flatten(name='FlattenUsers')(user_embedding)

prod = keras.layers.dot([movie_vec, user_vec], axes=1)
# prod = keras.layers.Dense(1, activation=tf.nn.relu)(prod)
model = keras.Model([user_input, movie_input], prod)
model.compile('adam', 'mean_squared_error')
model.summary()
history = model.fit([train.user_id, train.item_id], train.rating, epochs=10, verbose=1)
pd.Series(history.history['loss']).plot(logy=True)
plt.xlabel("Epoch")
plt.ylabel("Train Error")
y_train = model.predict([test.user_id, test.item_id])
y_train = np.round(y_train, 0)
y_true = test.rating
res = mean_absolute_error(y_true, y_train)
print(res)
plt.show()
